# Slack

This is a PCMRBot.js module that facilitates communication with the Slack API.

There is no suggested limit to the number of instances of this model you can run, but if you have multiple hosts hosting the actions/events APIs, you will need to run a load balancer in front of them to spread the load properly.

## Dependencies

This module has no dependencies. 

## Environment variables

The following environment variables should be set prior to launch. 

* Module options
  * `SLACK_TOKEN` - The TOTP token assigned by Slack for your application
  * (optional) `SLACK_DEFAULTCHANNEL` - The default channel to post to if no channel is supplied
  * (optional) `SLACK_USE_ACTIONS_API` - Whether a server for the Actions API should be launched. Defaults to false
  * (optional) `SLACK_USE_EVENTS_API` - Whether a server for the Events API should be launched. Defaults to false
  * (optional) `SLACK_ACTIONS_PORT` - The port to use for the Actions API server. Not required if server is not enabled
  * (optional) `SLACK_EVENTS_PORT` - The port to use for the Events API server. Not required if server is not enabled
  * (optional) `SLACK_ACTIONS_HOST` - The hostname of the machine the Actions API server is running on. Used for proxy/load balancing configuration
  * (optional) `SLACK_EVENTS_HOST` - The hostname of the machine the Events API server is running on. Used for proxy/load balancing configuration
  * (optional) `SLACK_VERIFICATION_TOKEN` - The Actions API verification token. Not required if the Actions API is not enabled
* Broker options
  * (optional) `TRANSPORT_NODE_ID` - The node ID that should be used for connecting to the rest of the cluster. Defaults to the system hostname plus a random number string. 
  * (optional) `TRANSPORT_BIND_ADDRESS` - The network address that the service broker should bind to. Defaults to all available addresses across all network interfaces.

# API Reference

## Modules

<dl>
<dt><a href="#module_slack.actions">slack.actions</a></dt>
<dd><p>This module emits events whenever actions are received from Slack webhooks. Event names are in the format <code>slack.actions.&lt;eventType&gt;</code>, where <code>&lt;eventType&gt;</code> is defined within the Slack application settings. All actions receive a HTTP 200 response; any responses to the action should use means other than replying to the action call.</p>
<p>If enabled, this module spins up a web server on a port specified using environment variables.</p>
<p>While the <code>receiveAction</code> action is publicly exposed, it is not intended to be used by other packages. It is documented here for completeness.</p>
</dd>
<dt><a href="#module_slack.commands">slack.commands</a></dt>
<dd><p>This module emits events whenever commands are received from Slack webhooks. Event names are in the format <code>slack.actions.&lt;eventType&gt;</code>, where <code>&lt;eventType&gt;</code> is the command invoked in Slack without the leading <code>/</code>. All commands receive a HTTP 200 response; any responses to the command should use means other than replying to the command call.</p>
<p>If enabled, this module spins up a web server on a port specified using environment variables.</p>
<p>While the <code>receiveCommand</code> action is publicly exposed, it is not intended to be used by other packages. It is documented here for completeness.</p>
</dd>
<dt><a href="#module_slack.events">slack.events</a></dt>
<dd><p>This module emits events whenever events are received from Slack webhooks. Event names are in the format <code>slack.events.&lt;eventType&gt;</code>, where <code>&lt;eventType&gt;</code> is defined within the Slack application settings. All actions receive a HTTP 200 response.</p>
<p>If enabled, this module spins up a web server on a port specified using environment variables.</p>
<p>While the <code>receiveEvent</code> action is publicly exposed, it is not intended to be used by other packages. It is documented here for completeness.</p>
</dd>
<dt><a href="#module_slack.web">slack.web</a></dt>
<dd><p>This module exposes utilities to work with the Slack Web API.</p>
</dd>
</dl>

<a name="module_slack.actions"></a>

## slack.actions
This module emits events whenever actions are received from Slack webhooks. Event names are in the format `slack.actions.<eventType>`, where `<eventType>` is defined within the Slack application settings. All actions receive a HTTP 200 response; any responses to the action should use means other than replying to the action call.If enabled, this module spins up a web server on a port specified using environment variables.While the `receiveAction` action is publicly exposed, it is not intended to be used by other packages. It is documented here for completeness.

**Version**: 1  
<a name="module_slack.actions.receiveAction"></a>

### slack.actions.receiveAction()
Listens to actions received from the Slack API, responds with a HTTP 200, and emits an event whose payload is the action details.

**Kind**: static method of [<code>slack.actions</code>](#module_slack.actions)  
**Throws**:

- Throws an exception if the verification token in the request and the one set using environment variables do not match.

**Access**: package  
<a name="module_slack.commands"></a>

## slack.commands
This module emits events whenever commands are received from Slack webhooks. Event names are in the format `slack.actions.<eventType>`, where `<eventType>` is the command invoked in Slack without the leading `/`. All commands receive a HTTP 200 response; any responses to the command should use means other than replying to the command call.If enabled, this module spins up a web server on a port specified using environment variables.While the `receiveCommand` action is publicly exposed, it is not intended to be used by other packages. It is documented here for completeness.

**Version**: 1  
<a name="module_slack.commands.receiveCommand"></a>

### slack.commands.receiveCommand()
Listens to commands received from the Slack API, responds with a HTTP 200, and emits an event whose payload is the command details.

**Kind**: static method of [<code>slack.commands</code>](#module_slack.commands)  
**Throws**:

- Throws an exception if the verification token in the request and the one set using environment variables do not match.

**Access**: package  
<a name="module_slack.events"></a>

## slack.events
This module emits events whenever events are received from Slack webhooks. Event names are in the format `slack.events.<eventType>`, where `<eventType>` is defined within the Slack application settings. All actions receive a HTTP 200 response.If enabled, this module spins up a web server on a port specified using environment variables.While the `receiveEvent` action is publicly exposed, it is not intended to be used by other packages. It is documented here for completeness.

**Version**: 1  
<a name="module_slack.events.receiveEvent"></a>

### slack.events.receiveEvent()
Listens to events received from the Slack API, responds with a HTTP 200, and emits an event whose payload is the event details.

**Kind**: static method of [<code>slack.events</code>](#module_slack.events)  
**Access**: package  
<a name="module_slack.web"></a>

## slack.web
This module exposes utilities to work with the Slack Web API.

**Version**: 1  

* [slack.web](#module_slack.web)
    * [.postMessage([message], [channel], [parent], [reply_broadcast], [attachments], [blocks])](#module_slack.web.postMessage) ⇒ <code>object</code>
    * [.postEphemeral(channel, user, [message], [attachments], [blocks])](#module_slack.web.postEphemeral) ⇒
    * [.updateMessage(channel, ts, [message], [attachments], [blocks])](#module_slack.web.updateMessage) ⇒ <code>object</code>
    * [.deleteMessage(channel, ts)](#module_slack.web.deleteMessage) ⇒ <code>object</code>
    * [.openDialog(callbackId, triggerId, title, submitLabel, elements, [notifyOnCancel])](#module_slack.web.openDialog) ⇒ <code>object</code>
    * [.createFile(channels, content, [filename], [filetype], [parent], [title])](#module_slack.web.createFile) ⇒ <code>object</code>

<a name="module_slack.web.postMessage"></a>

### slack.web.postMessage([message], [channel], [parent], [reply_broadcast], [attachments], [blocks]) ⇒ <code>object</code>
Posts a message to the specified channel.

**Kind**: static method of [<code>slack.web</code>](#module_slack.web)  
**Returns**: <code>object</code> - The API response  
**Throws**:

- Throws an error if the message, attachments, and blocks are omitted, or if no default channel is set.


| Param | Type | Default | Description |
| --- | --- | --- | --- |
| [message] | <code>string</code> |  | The text string to be posted to Slack. Not required if attachments or blocks are provided. |
| [channel] | <code>string</code> |  | The channel the message will be posted into. Not required if the default channel is set |
| [parent] | <code>string</code> |  | The timestamp of the message you want to reply to |
| [reply_broadcast] | <code>boolean</code> | <code>false</code> | Whether the message should be broadcast to the channel, if it is a reply to a thread. |
| [attachments] | <code>Array.&lt;object&gt;</code> |  | The attachments array. Not required if a message or blocks are supplied. Usage is discouraged in favor of blocks |
| [blocks] | <code>Array.&lt;object&gt;</code> |  | The blocks array. Not required if a message or attachments are supplied |

<a name="module_slack.web.postEphemeral"></a>

### slack.web.postEphemeral(channel, user, [message], [attachments], [blocks]) ⇒
Posts a message to a specific user in a channel.

**Kind**: static method of [<code>slack.web</code>](#module_slack.web)  
**Returns**: The API response  
**Throws**:

- Throws if the message, attachments, and blocks are undefined


| Param | Type | Description |
| --- | --- | --- |
| channel | <code>string</code> | The channel the message will be posted into |
| user | <code>string</code> | The recipient of the message |
| [message] | <code>string</code> | The message text. Not required if attachments or blocks are supplied |
| [attachments] | <code>Array.&lt;object&gt;</code> | The attachments array. Not required if a message or blocks are supplied. Usage is discouraged in favor of blocks |
| [blocks] | <code>Array.&lt;object&gt;</code> | The blocks array. Not required if a message or attachments are supplied |

<a name="module_slack.web.updateMessage"></a>

### slack.web.updateMessage(channel, ts, [message], [attachments], [blocks]) ⇒ <code>object</code>
Updates the contents of a message.

**Kind**: static method of [<code>slack.web</code>](#module_slack.web)  
**Returns**: <code>object</code> - The API response from Slack  
**Throws**:

- <code>InvalidParameterException</code> Throws an error if the channel or timestamp are omitted, or if both the text and attachments are omitted.


| Param | Type | Description |
| --- | --- | --- |
| channel | <code>string</code> | The channel that the message to be updated is in |
| ts | <code>string</code> | The timestamp of the message to be updated |
| [message] | <code>string</code> | The message text. Not required if attachments are provided |
| [attachments] | <code>Array.&lt;object&gt;</code> | The attachments array. Not required if a message or blocks are supplied. Usage is discouraged in favor of blocks |
| [blocks] | <code>Array.&lt;object&gt;</code> | The blocks array. Not required if a message or attachments are supplied |

<a name="module_slack.web.deleteMessage"></a>

### slack.web.deleteMessage(channel, ts) ⇒ <code>object</code>
Deletes a message.

**Kind**: static method of [<code>slack.web</code>](#module_slack.web)  
**Returns**: <code>object</code> - The API response from Slack  

| Param | Type | Description |
| --- | --- | --- |
| channel | <code>string</code> | The channel that the message to be deleted resides in |
| ts | <code>string</code> | The timestamp of the message to be deleted |

<a name="module_slack.web.openDialog"></a>

### slack.web.openDialog(callbackId, triggerId, title, submitLabel, elements, [notifyOnCancel]) ⇒ <code>object</code>
Creates a new dialog to the user.

**Kind**: static method of [<code>slack.web</code>](#module_slack.web)  
**Returns**: <code>object</code> - The raw API response. Should be `{ok: true}` most of the time  

| Param | Type | Default | Description |
| --- | --- | --- | --- |
| callbackId | <code>string</code> |  | The callback ID |
| triggerId | <code>string</code> |  | The trigger ID. This can be obtained in an action callback |
| title | <code>string</code> |  | The form title |
| submitLabel | <code>string</code> |  | The string to use for the submit button |
| elements | <code>Array.&lt;object&gt;</code> |  | The stream elements to include |
| [notifyOnCancel] | <code>boolean</code> | <code>false</code> | Whether a callback should be received on dialog close |

<a name="module_slack.web.createFile"></a>

### slack.web.createFile(channels, content, [filename], [filetype], [parent], [title]) ⇒ <code>object</code>
Creates a new file and shares it with the specified channels.

**Kind**: static method of [<code>slack.web</code>](#module_slack.web)  
**Returns**: <code>object</code> - A new file object as returned by the Slack API.  

| Param | Type | Default | Description |
| --- | --- | --- | --- |
| channels | <code>Array.&lt;string&gt;</code> |  | An array of channel IDs |
| content | <code>string</code> |  | The file content |
| [filename] | <code>string</code> |  | The filename to give the new file |
| [filetype] | <code>string</code> | <code>&quot;auto&quot;</code> | The file type the file is. Possible file types listed here: https://api.slack.com/types/file#file_types |
| [parent] | <code>string</code> |  | The timestamp of the message to attach this to as a reply |
| [title] | <code>string</code> |  | The title for the new file |

