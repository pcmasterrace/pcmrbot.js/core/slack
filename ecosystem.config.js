module.exports = {
	apps : [{
		name      : 'core/slack',
		script    : 'build/slack.js',
		env: {
			// Either inject the values via environment variables or define them here
			TRANSPORT_BIND_ADDRESS: process.env.TRANSPORT_BIND_ADDRESS || "",
            SLACK_TOKEN: process.env.SLACK_TOKEN || "",
            SLACK_VERIFICATION_TOKEN: process.env.SLACK_VERIFICATION_TOKEN || "",
            SLACK_USE_EVENTS_API: process.env.SLACK_USE_EVENTS_API || false,
            SLACK_EVENTS_PORT: process.env.SLACK_EVENTS_PORT || "",
            SLACK_USE_ACTIONS_API: process.env.SLACK_USE_ACTIONS_API || false,
            SLACK_ACTIONS_PORT: process.env.SLACK_ACTIONS_PORT || ""
		}
	}]
};
