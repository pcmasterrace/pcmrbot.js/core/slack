import { Service, Context, Errors } from "moleculer";
import { WebClient } from "@slack/client";

/**
 * This module exposes utilities to work with the Slack Web API.
 * 
 * @module "slack.web"
 * @version 1
 */
class WebApiService extends Service {
	protected slackWeb: WebClient;

	constructor(broker) {
		super(broker);

		this.parseServiceSchema({
			name: "slack.web",
			version: 1,
			actions: {
				postMessage: {
					name: "postMessage",
					params: {
						message: {type: "string", optional: true},
						attachments: {type: "array", optional: true},
						blocks: {type: "array", optional: true},
						channel: {type: "string", optional: true},
						parent: {type: "string", optional: true}
					},
					handler: this.postMessage
				},
				postEphemeral: {
					name: "postEphemeral",
					params: {
						channel: "string",
						user: "string",
						message: {type: "string", optional: true},
						attachments: {type: "array", optional: true},
						blocks: {type: "array", optional: true}
					},
					handler: this.postEphemeral
				},
				updateMessage: {
					name: "updateMessage",
					params: {
						channel: "string",
						ts: "string",
						message: {type: "string", optional: true},
						attachments: {type: "array", optional: true},
						blocks: {type: "array", optional: true}
					},
					handler: this.updateMessage
				},
				deleteMessage: {
					name: "deleteMessage",
					params: {
						channel: "string",
						ts: "string"
					},
					handler: this.deleteMessage
				},
				openDialog: {
					name: "openDialog",
					params: {
						callbackId: "string",
						triggerId: "string",
						title: "string",
						submitLabel: "string",
						elements: "object",
						notifyOnCancel: {type: "boolean", optional: true}
					},
					handler: this.openDialog
				},
				createFile: {
					name: "createFile",
					params: {
						channels: {type: "array", min: 1, items: "string"},
						content: "string",
						filename: {type: "string", optional: true},
						filetype: {type: "string", optional: true},
						thread_ts: {type: "string", optional: true},
						title: {type: "string", optional: true}
					},
					handler: this.createFile
				}
			},
			created: this.serviceCreated
		});
	}

	/**
	 * Posts a message to the specified channel.
	 * @function
	 * @static
	 * @name postMessage
	 * @param {string} [message] - The text string to be posted to Slack. Not required if attachments or blocks are provided.
	 * @param {string} [channel] - The channel the message will be posted into. Not required if the default channel is set
	 * @param {string} [parent] - The timestamp of the message you want to reply to
	 * @param {boolean} [reply_broadcast=false] - Whether the message should be broadcast to the channel, if it is a reply to a thread.
	 * @param {object[]} [attachments] - The attachments array. Not required if a message or blocks are supplied. Usage is discouraged in favor of blocks
	 * @param {object[]} [blocks] - The blocks array. Not required if a message or attachments are supplied
	 * @returns {object} The API response
	 * @throws Throws an error if the message, attachments, and blocks are omitted, or if no default channel is set.
	 */
	async postMessage(ctx: Context) {
		// TODO: Perform type checking on these
		let message = ctx.params.message || undefined;
		let channel = ctx.params.channel || process.env.SLACK_DEFAULTCHANNEL;
		let thread_ts = ctx.params.parent || undefined;
		let reply_broadcast = ctx.params.reply_broadcast || false;
		let attachments = ctx.params.attachments || undefined;
		let blocks = ctx.params.blocks || undefined;
		let username = ctx.params.username || undefined;

		if (message === undefined && attachments === undefined && blocks == undefined) {
			throw new Errors.MoleculerClientError("No message defined, you dingus", 422, "MISSING_PARAM_ERROR");
		}

		return await this.slackWeb.chat.postMessage({
			channel: channel,
			text: message,
			thread_ts: thread_ts,
			reply_broadcast: reply_broadcast,
			attachments: attachments,
			blocks: blocks,
			username: username
		});
	}

	/**
	 * Posts a message to a specific user in a channel.
	 * @function
	 * @static
	 * @name postEphemeral
	 * @param {string} channel - The channel the message will be posted into
	 * @param {string} user - The recipient of the message
	 * @param {string} [message] - The message text. Not required if attachments or blocks are supplied
	 * @param {object[]} [attachments] - The attachments array. Not required if a message or blocks are supplied. Usage is discouraged in favor of blocks
	 * @param {object[]} [blocks] - The blocks array. Not required if a message or attachments are supplied
	 * @returns The API response
	 * @throws Throws if the message, attachments, and blocks are undefined
	 */
	async postEphemeral(ctx: Context) {
		// TODO: Perform type checking on these
		let message = ctx.params.message;
		let channel = ctx.params.channel;
		let user = ctx.params.user;
		let attachments = ctx.params.attachments;
		let blocks = ctx.params.blocks;

		if (message === undefined && attachments === undefined && blocks === undefined) {
			throw new Errors.MoleculerClientError("No message defined, you dingus", 422, "MISSING_PARAM_ERROR");
		}

		return await this.slackWeb.chat.postEphemeral({
			channel: channel,
			text: message,
			user: user,
			attachments: attachments,
			blocks: blocks,
			parse: "full"
		});
	}
	 /**
	 * Updates the contents of a message.
	 * @function
	 * @static
	 * @name updateMessage
	 * @param {string} channel - The channel that the message to be updated is in
	 * @param {string} ts - The timestamp of the message to be updated
	 * @param {string} [message] - The message text. Not required if attachments are provided
	 * @param {object[]} [attachments] - The attachments array. Not required if a message or blocks are supplied. Usage is discouraged in favor of blocks
	 * @param {object[]} [blocks] - The blocks array. Not required if a message or attachments are supplied
	 * @return {object} The API response from Slack
	 * @throws {InvalidParameterException} Throws an error if the channel or timestamp are omitted, or if both the text and attachments are omitted.
	 */
	async updateMessage(ctx: Context) {
		let channel = ctx.params.channel;
		let ts = ctx.params.ts;
		let message = ctx.params.message;
		let attachments = ctx.params.attachments;
		let blocks = ctx.params.blocks;

		if (message === undefined && attachments === undefined && blocks === undefined) {
			throw new Errors.MoleculerClientError("No message contents defined, you dingus", 422, "MISSING_PARAM_ERROR");
		}

		return await this.slackWeb.chat.update({
			channel: channel,
			ts: ts,
			text: message,
			attachments: attachments,
			blocks: blocks,
			link_names: true
		});
	}

	/**
	 * Deletes a message.
	 * @function
	 * @static
	 * @name deleteMessage
	 * @param {string} channel - The channel that the message to be deleted resides in
	 * @param {string} ts - The timestamp of the message to be deleted
	 * @returns {object} The API response from Slack
	 */
	async deleteMessage(ctx: Context) {
		let response = await this.slackWeb.chat.delete({
			channel: ctx.params.channel,
			ts: ctx.params.ts
		});

		return response;
	}

	/**
	 * Creates a new dialog to the user.
	 * @function
	 * @static
	 * @name openDialog
	 * @param {string} callbackId - The callback ID
	 * @param {string} triggerId - The trigger ID. This can be obtained in an action callback
	 * @param {string} title - The form title
	 * @param {string} submitLabel - The string to use for the submit button
	 * @param {object[]} elements - The stream elements to include
	 * @param {boolean} [notifyOnCancel=false] - Whether a callback should be received on dialog close
	 * @returns {object} The raw API response. Should be `{ok: true}` most of the time
	 */
	async openDialog (ctx: Context) {
		let response = await this.slackWeb.dialog.open({
			dialog: {
				callback_id: ctx.params.callbackId,
				title: ctx.params.title,
				submit_label: ctx.params.submitLabel,
				elements: ctx.params.elements,
				notify_on_cancel: ctx.params.notifyOnCancel || false
			},
			trigger_id: ctx.params.triggerId
		});

		return response;
	}

	/**
	 * Creates a new file and shares it with the specified channels.
	 * @function
	 * @static
	 * @name createFile
	 * @param {string[]} channels - An array of channel IDs
	 * @param {string} content - The file content
	 * @param {string} [filename] - The filename to give the new file
	 * @param {string} [filetype=auto] - The file type the file is. Possible file types listed here: https://api.slack.com/types/file#file_types
	 * @param {string} [parent] - The timestamp of the message to attach this to as a reply
	 * @param {string} [title] - The title for the new file
	 * @returns {object} A new file object as returned by the Slack API.
	 */
	async createFile (ctx: Context) {
		return await this.slackWeb.files.upload({
			channels: ctx.params.channels,
			content: ctx.params.content,
			filename: ctx.params.filename || undefined,
			filetype: ctx.params.filetype || "auto",
			thread_ts: ctx.params.parent || undefined,
			title: ctx.params.title || undefined
		});
	}

	serviceCreated() {
		this.slackWeb = new WebClient(process.env.SLACK_TOKEN);
	}
}

module.exports = WebApiService;