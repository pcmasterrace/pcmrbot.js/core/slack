import { Service, Context, Errors } from "moleculer";
import * as ApiService from "moleculer-web";
import * as yn from "yn";
import * as os from "os";

const pkg = require("../package.json");

const errorCodes = {
	NO_BODY_PARSER: 'MOLECULERSLACKACTIONS_NO_BODY_PARSER',
	TOKEN_VERIFICATION_FAILURE: 'MOLECULERSLACKACTIONS_TOKEN_VERIFICATION_FAILURE',
}

const responseStatuses = {
	OK: 200,
	FAILURE: 500,
	REDIRECT: 302
}

/**
 * This module emits events whenever actions are received from Slack webhooks. Event names are in the format `slack.actions.<eventType>`, where `<eventType>` is defined within the Slack application settings. All actions receive a HTTP 200 response; any responses to the action should use means other than replying to the action call. 
 * 
 * If enabled, this module spins up a web server on a port specified using environment variables.
 * 
 * While the `receiveAction` action is publicly exposed, it is not intended to be used by other packages. It is documented here for completeness.
 * 
 * @module "slack.actions"
 * @version 1
 */
class ActionsApiService extends Service {
	constructor(broker) {
		super(broker);

		this.parseServiceSchema({
			name: "slack.actions",
			version: 1,
			mixins: yn(process.env.SLACK_USE_ACTIONS_API, {default: false}) ? [ApiService] : [],
			actions: {
				receiveAction: {
					name: "receiveAction",
					handler: this.receiveAction
				}
			},
			settings: {
				port: process.env.SLACK_ACTIONS_PORT || 9002,
				verificationToken: process.env.SLACK_VERIFICATION_TOKEN,
				routes: [{
					aliases: {
						"POST /": "v1.slack.actions.receiveAction"
					},
					bodyParsers: {
						json: true,
						urlencoded: {extended: true}
					},
					mappingPolicy: "restrict"
				}]
			}
		})
	}

	/**
	 * Listens to actions received from the Slack API, responds with a HTTP 200, and emits an event whose payload is the action details.
	 * @function
	 * @package
	 * @static
	 * @name receiveAction
	 * @throws Throws an exception if the verification token in the request and the one set using environment variables do not match.
	 */
	async receiveAction(ctx: Context) {        
		// Set response headers
		ctx.meta.$responseHeaders = {
			"X-Slack-Powered-By": this.packageIdentifier()
		};

		// Request token verification
		if (!ctx.params.token || ctx.params.token !== this.settings.verificationToken) {
			this.logger.debug("Request token verification failure");
			const error = new Errors.MoleculerError("Slack action verification failed");
			error.name = errorCodes.TOKEN_VERIFICATION_FAILURE;
			error.data = ctx.params;
			ctx.meta.$statusCode = responseStatuses.FAILURE;
			throw error;
		}
		this.logger.debug("Request token verification success");

		this.broker.emit(`${this.name}.${ctx.params.event.type}`, ctx.params);
	}

	packageIdentifier(): string {
		return `${pkg.name.replace("/", ":")}/${pkg.version} ${os.platform()}/${os.release()} node/${process.version.replace("v", "")}`;
	}
}

module.exports = ActionsApiService;