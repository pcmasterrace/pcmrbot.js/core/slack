import { Service, Context, Errors } from "moleculer";
import * as ApiService from "moleculer-web";
import * as yn from "yn";
import * as os from "os";

const pkg = require("../package.json");

const errorCodes = {
	NO_BODY_PARSER: 'MOLECULERSLACKEVENTS_NO_BODY_PARSER',
	TOKEN_VERIFICATION_FAILURE: 'MOLECULERSLACKEVENTS_TOKEN_VERIFICATION_FAILURE',
}

const responseStatuses = {
	OK: 200,
	FAILURE: 500,
	REDIRECT: 302
}

/**
 * This module emits events whenever events are received from Slack webhooks. Event names are in the format `slack.events.<eventType>`, where `<eventType>` is defined within the Slack application settings. All actions receive a HTTP 200 response. 
 * 
 * If enabled, this module spins up a web server on a port specified using environment variables.
 * 
 * While the `receiveEvent` action is publicly exposed, it is not intended to be used by other packages. It is documented here for completeness.
 * 
 * @module "slack.events"
 * @version 1
 */
class EventsApiService extends Service {
	constructor(broker) {
		super(broker);
		
		this.parseServiceSchema({
			name: "slack.events",
			version: 1,
			mixins: yn(process.env.SLACK_USE_EVENTS_API, {default: false}) ? [ApiService] : [],
			actions: {
				receiveEvent: {
					name: "receiveEvent",
					handler: this.receiveEvent
				}
			},
			settings: {
				port: process.env.SLACK_EVENTS_PORT || 9001,
				verificationToken: process.env.SLACK_VERIFICATION_TOKEN,
				routes: [{
					aliases: {
						"POST /": "v1.slack.events.receiveEvent"
					},
					bodyParsers: {
						json: true,
						urlencoded: {extended: true}
					},
					mappingPolicy: "restrict"
				}]
			}
		})
	}

	/**
	 * Listens to events received from the Slack API, responds with a HTTP 200, and emits an event whose payload is the event details.
	 * @function
	 * @package
	 * @static
	 * @name receiveEvent
	 */
	async receiveEvent(ctx: Context) {        
		// Set response headers
		ctx.meta.$responseHeaders = {
			"X-Slack-Powered-By": this.packageIdentifier()
		};

		// URL verification challenge
		if(ctx.params.type === "url_verification") {
			this.logger.debug("Handling URL verification");
			if (ctx.params.token !== this.settings.verificationToken) {
				this.logger.debug("URL verification failure");
				const error = new Errors.MoleculerError("Slack event challenge failed");
				error.name = errorCodes.TOKEN_VERIFICATION_FAILURE;
				error.data = ctx.params;
				ctx.meta.$statusCode = responseStatuses.FAILURE;
				throw error;
			}
			this.logger.debug("URL verification success");
			
			return ctx.params.challenge;
		}

		// Request token verification
		if (!ctx.params.token || ctx.params.token !== this.settings.verificationToken) {
			this.logger.debug("Request token verification failure");
			const error = new Errors.MoleculerError("Slack event verification failed");
			error.name = errorCodes.TOKEN_VERIFICATION_FAILURE;
			error.data = ctx.params;
			ctx.meta.$statusCode = responseStatuses.FAILURE;
			throw error;
		}
		this.logger.debug("Request token verification success");

		this.broker.emit(`${this.name}.${ctx.params.event.type}`, ctx.params);
	}

	packageIdentifier(): string {
		return `${pkg.name.replace("/", ":")}/${pkg.version} ${os.platform()}/${os.release()} node/${process.version.replace("v", "")}`;
	}
}

module.exports = EventsApiService;